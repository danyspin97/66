#include <stdlib.h>

#include "66/environ.h"

#include "EMUtest.h"

EMU_TEST(env_parse_key) {
	char *mykey = "ENV1";
	bool unexport;
	EMU_EXPECT_TRUE(env_parse_key(mykey, &unexport));
	EMU_EXPECT_FALSE(unexport);
	EMU_END_TEST();
}

EMU_TEST(env_parse_key_whitespace) {
	char *mykey = "ENV 1";
	bool unexport;
	EMU_EXPECT_FALSE(env_parse_key(mykey, &unexport));
	EMU_END_TEST();
}

EMU_TEST(env_parse_key_whitespaces) {
	char *mykey = "ENV   \t1";
	bool unexport;
	EMU_EXPECT_FALSE(env_parse_key(mykey, &unexport));
	EMU_END_TEST();
}

EMU_TEST(env_parse_key_unexport) {
	char *mykey = "!ENV1";
	bool unexport;
	EMU_EXPECT_TRUE(env_parse_key(mykey, &unexport));
	EMU_EXPECT_TRUE(unexport);
	EMU_END_TEST();
}

EMU_TEST(env_parse_key_unexport_whitespaces) {
	char *mykey = "! ENV1";
	bool unexport;
	EMU_EXPECT_TRUE(env_parse_key(mykey, &unexport));
	EMU_EXPECT_TRUE(unexport);
	EMU_END_TEST();
}

EMU_TEST(env_entry_split) {
	char *myenv = "ENV1=test_env_1";
	struct env_entry entry;

	EMU_EXPECT_TRUE(env_entry_split(&entry, myenv));

	EMU_EXPECT_STREQ(entry.key, "ENV1");
	EMU_EXPECT_STREQ(entry.value, "test_env_1");

	free(entry.key);
	free(entry.value);
	EMU_END_TEST();
}

EMU_TEST(env_entry_split_malloc) {
	char *myenv = "ENV1=test_env_1";
	struct env_entry *entry = malloc(sizeof(struct env_entry));

	EMU_EXPECT_TRUE(env_entry_split(entry, myenv));

	EMU_EXPECT_STREQ(entry->key, "ENV1");
	EMU_EXPECT_STREQ(entry->value, "test_env_1");

	free(entry);
	EMU_END_TEST();
}

EMU_TEST(env_entry_split_spaces_in_key) {
	char *myenv = "ENV 1=test_env_1";
	struct env_entry entry;

	EMU_EXPECT_FALSE(env_entry_split(&entry, myenv));
	EMU_END_TEST();
}

int main() {
	EMU_RUN(env_parse_key);
	EMU_RUN(env_parse_key_whitespace);
	EMU_RUN(env_parse_key_whitespaces);
	EMU_RUN(env_parse_key_unexport);
	EMU_RUN(env_parse_key_unexport_whitespaces);
	EMU_RUN(env_entry_split);
	EMU_RUN(env_entry_split_malloc);
	return 0;
}

