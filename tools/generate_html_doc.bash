#!/bin/bash

generate_page() {
    scdoc < $1 | \
        sed -e 's,%%skeldir%%,@/etc/66@,'\
            -e 's,%%livedir%%,@/run/66@,'\
            -e 's,%%systemdir%%,@/var/lib/66@,'\
            -e 's,%%userdir%%,@.66@,'\
            -e 's,%%service-conf%%,@/etc/66/conf@,'\
            -e 's,%%user-conf%%,@.66/conf@,'\
            -e 's,%%system-service%%,@/usr/share/66/service@,g'\
            -e 's,%%userlog%%,@.66/log@,'\
            -e 's,%%sysadmin-service%%,@/etc/66/service@,'\
            -e 's,%%systemlog%%,@/var/log/66@,'\
            -e 's,%%user-service%%,@.66/service@,' | \
        man2html -r | \
        sed -e '1,4d' \
        	-e 's,\.\./man[1-8]/,,'\
            -e 's,\.\./\(index.html\),\1,' >> $2
}

if [[ -d build/html ]] ; then
    rm -rf build/html
fi
if [[ ! -d build/html ]] ; then
    mkdir -p build/html
fi

for i in man/*.scd ; do
    filename=$(basename -- "$i")
    filename="${filename%.*}"
    output=build/html/${filename}.html
    cat tools/head.html.part > $output
    generate_page $i $output
done

