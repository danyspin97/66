66-echo(1)

# NAME

66-echo - An exact copy of s6-echo program

## DESCRIPTION
Exact copy of *s6-echo*. Adapted to the 66 tools suite to avoid a dependency on
s6-portable-utils at compilation time of *66*. Details and further information
on the command can be found at the *s6-echo* documentation page.
