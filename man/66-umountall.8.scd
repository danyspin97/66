66-umountall(8)

# NAME

66-umountall - umount all filesystems

# DESCRIPTION

*66-umountall* is an exact copy of *s6-linux-init-umountall* program. It is
was adapted to avoid the dependencies on *s6-linux-init* package at tools compile time. Refers to the *s6-linux-init* documentation page for
futher information.
